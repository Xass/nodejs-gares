var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;

MongoClient.connect('mongodb://localhost:27017/examD2D13', { useNewUrlParser: true }, function (err, client) {
  if (err) throw err

  let db = client.db('examD2D13')
  /* GET home page. */
  router.get('/', function(req, res, next) {
    console.log('here');
    db.collection('gares').find({}).toArray(function (err, result) {
      res.render('index', { title: 'Liste', gares: result });
      console.log(result);
    })
  });
})
module.exports = router;
