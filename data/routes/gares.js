var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;

MongoClient.connect('mongodb://localhost:27017/examD2D13', { useNewUrlParser: true }, function (err, client) {
  if (err) throw err
  router.delete(':id', function(req, res, next) {
    db.collection('gares').deleteOne({_id : ObjectId(req.params.id)},function (err, result) {
      if(err) return next(err);
      return res.json(result);
    })
  });

  router.post('/', function(req, res, next){
    var gare = {
        name : req.body.name,
        datecreation : new Date(),
        siret : req.body.siret,
        theme : 'DEPLACEMENT',
        idexterne : '',
        soustheme : 'Gare ferroviaire',
        gid : '',
        identifiant : '',
    }
    db.collection('gares').insertOne(gare, function(err, result){
        return res.json(result) ;
    })
  })
})
module.exports = router;
