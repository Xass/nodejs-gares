var setDelete = function(element){
    element.addEventListener('click', function() {
      makeRequest(this.getAttribute('data-id'), 'DELETE', null, function(res) {
         alert('effacé');
         element.closest('aside').remove();
      });
    })
};
document.querySelectorAll('.delete').forEach(setDelete);

function makeRequest(id, method, datas, callback) {
    httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
      if (httpRequest.readyState === XMLHttpRequest.DONE) {
          if (httpRequest.status === 200) {
            callback(JSON.parse(httpRequest.responseText));
          } else {
            alert('There was a problem with the request.');
          }
        }
    };
     httpRequest.open(method, '/gares/' + id);
    httpRequest.setRequestHeader('Content-Type', 'application/json');
    httpRequest.send(datas ? JSON.stringify(datas) : null);
 }
